# * m0rfeo zsh config
# * Last Update: 20/01/23
#  _______________
# |GENERAL OPTIONS|
#  ---------------

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

CASE_SENSITIVE="true"						# Distinguish uppercase and lowercase.
zstyle ':omz:update' mode reminder  				# Just remind me to update when it's time.
DISABLE_AUTO_TITLE="true"

#  _______
# |EXPORTS|
#  -------

export PATH=$PATH:/sbin:/usr/sbin:/opt:$HOME/.poetry/bin
export ZSH="$HOME/.oh-my-zsh"       				
export LANG=es_ES.UTF-8						
export ARCHFLAGS="-arch x86_64"
export EDITOR='nvim'

#  _______
# |PLUGINS|
#  -------

# Standard plugins in $ZSH/plugins/
plugins=(git sudo fzf colored-man-pages extract history z zsh-interactive-cd zsh-navigation-tools zsh-autosuggestions zsh-syntax-highlighting)
source $ZSH/oh-my-zsh.sh

#  _________
# |FUNCTIONS|
#  ---------

# Kill processes with the name of the application. (Fixed to always delete parent process by m0rfeo so many years later).
pskill()
{
        local pid

        pid=$(ps ax | grep $1 | grep -v grep | awk '{ print $1 }' | head -n1)
	if [ -z  `echo $pid |awk '{ print $1 }'` ]
	then
	   echo "Lo siento, no encuentro la aplicacion '$1'"
	else
 	  for pidn in $pid
	  do
            echo -n "Aniquilando $1 (proceso $pidn)..."
             kill -9 $pidn 
             echo "fulminado." 
          done
	fi
}

# List user processes passed as firts argument.
psuser()
{
        ps aux | grep $1 | grep -v grep 
}

# Recursive grep
grepr()
{
 find . -name "*" | xargs egrep -l "$1" 2>/dev/null
}

# Set/Delete target on polybar.
set-target(){
    case $1 in
        -d)
            rm /tmp/target.txt
        ;;
        *)
            echo $1 > /tmp/target.txt
        ;;
    esac
}

#__________________________________________________________________________________________________
# This block of functions is provided by s4vitar, thanks for a great content always.
# (https://www.youtube.com/watch?v=mHLwfI1nHHY)

# Create directory structure to work hacking machinese
mkt(){
	mkdir -p $1/{exploits,content,enum}
}

# Extract nmap information from a grepeable output.
extractPorts(){
	ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')"
	ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)"
	echo -e "\n[*] Extracting information...\n" > extractPorts.tmp
	echo -e "\t[*] IP Address: $ip_address"  >> extractPorts.tmp
	echo -e "\t[*] Open ports: $ports\n"  >> extractPorts.tmp
	echo $ports | tr -d '\n' | xclip -sel clip
	echo -e "[*] Ports copied to clipboard\n"  >> extractPorts.tmp
	batcat extractPorts.tmp; rm extractPorts.tmp
}

# Secure delete.
rmk(){
	scrub -p dod $1
	shred -zun 10 -v $1
}

# Upgrade fzf to display file content.
fzf-lovely(){
	if [ "$1" = "h" ]; then
		fzf -m --reverse --preview-window down:20 --preview '[[ $(file --mime {}) =~ binary ]] &&
 	                echo {} is a binary file ||
	                 (bat --style=numbers --color=always {} ||
	                  highlight -O ansi -l {} ||
	                  coderay {} ||
	                  rougify {} ||
	                  cat {}) 2> /dev/null | head -500'

	else
	        fzf -m --preview '[[ $(file --mime {}) =~ binary ]] &&
	                         echo {} is a binary file ||
	                         (bat --style=numbers --color=always {} ||
	                          highlight -O ansi -l {} ||
	                          coderay {} ||
	                          rougify {} ||
	                          cat {}) 2> /dev/null | head -500'
	fi
}

# Fix problem on vim with cursor.
zle-keymap-select(){
    if [[ $KEYMAP == vicmd ]] || [[ $1 == 'block' ]]; then
        echo -ne '\e[1 q'
    elif [[ $KEYMAP == main ]] || [[ $KEYMAP == viins ]] || [[ $KEYMAP == '' ]] || [[ $1 = 'beam' ]]; then
        echo -ne '\e[5 q'
    fi
}
zle -N zle-keymap-select
zle-line-init() { zle-keymap-select 'beam' }

#__________________________________________________________________________________________________
#  _____
# |ALIAS|
#  -----

alias cat=batcat        
alias ls=lsd            
alias fzf=fzf-lovely    
alias v=nvim		
alias crackmapexec='cd /opt/CrackMapExec && poetry run crackmapexec'
alias bloodhound='/opt/BloodHound-linux-x64/BloodHound-linux-x64/./BloodHound'
alias led='sudo python3 /opt/g213colors.py -c 00ff00 00ff00 00ff00 ff0051 ff0051'
alias clsh='echo "" > /home/m0rfeo/.zsh_history'
alias bpytop='/home/m0rfeo/.local/lib/python3.9/site-packages/bpytop.py'

# P10K THEME CONF
# !!!Wizard to configure!!!: p10k configure
source ~/powerlevel10k/powerlevel10k.zsh-theme
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
