# Neofetch
# * m0rfeo config
# * Last Update: 21/10/22

# Display
print_info() {
    echo ""
    prin "-------.SISTEMA.-------"
    info "Usuario" title
    info "Actividad" uptime
    info "Distro" distro
    info "Kernel" kernel
    info "Paquetería" packages
    info "Shell" shell
    info "Resolución" resolution
    info "Terminal" term
    info "WM" wm
    prin "------------------------"
    echo ""
    prin "-------.HARDWARE.-------"
    info "Host" model
    info "CPU" "cpu"
    info "Uso de CPU" cpu_usage
    info "RAM" memory
    info "Disco SSD" disk
    info "GPU" gpu
    prin "------------------------"
}

# Hide/Show Fully qualified domain name.
title_fqdn="on"

# Kernel
kernel_shorthand="on"

# Distro
distro_shorthand="off"

# Show/Hide OS Architecture.
os_arch="on"

# Uptime
# Values:   'on', 'tiny', 'off'
uptime_shorthand="off"

# Memory
memory_percent="on"

# Packages
# Values:  'on', 'tiny' 'off'
package_managers="on"

# Shell
shell_path="off"

# Show $SHELL version
shell_version="on"

# CPU
# CPU speed type
speed_type="bios_limit"

# CPU speed shorthand
speed_shorthand="off"

# Enable/Disable CPU brand in output.
cpu_brand="off"

# CPU Speed
cpu_speed="on"

# CPU Cores
cpu_cores="logical"

# CPU Temperature
cpu_temp="C"

# GPU
# Enable/Disable GPU Brand
gpu_brand="on"

# Which GPU to display
gpu_type="all"

# Resolution
refresh_rate="on"

# Gtk Theme / Icons / Font
gtk_shorthand="on"

# Enable/Disable gtk2 Theme / Icons / Font
gtk2="on"

# Enable/Disable gtk3 Theme / Icons / Font
gtk3="on"

# IP Address
public_ip_host="http://ident.me"

# Public IP timeout.
public_ip_timeout=2

# Desktop Environment
de_version="off"

# Disk
disk_show=('/')

# Disk subtitle.
disk_subtitle="mount"

# Disk percent.
disk_percent="on"

# 'mpc' arguments (specify a host, password etc).
# Example: mpc_args=(-h HOST -P PASSWORD)
mpc_args=()


# Text Colors
#
# Default:  'distro'
# Values:   'distro', 'num' 'num' 'num' 'num' 'num' 'num'
# Flag:     --colors
#
# Example:
# colors=(distro)      - Text is colored based on Distro colors.
# colors=(4 6 1 8 8 6) - Text is colored in the order above.
colors=(distro)

# Text Options
bold="on"

# Enable/Disable Underline
underline_enabled="on"

# Underline character
underline_char="-"

# Info Separator
separator=":"

block_range=(0 15)

# Toggle color blocks
color_blocks="on"

# Color block width in spaces
block_width=3

# Color block height in lines
block_height=1

# Color Alignment
col_offset="auto"


# Bar characters
bar_char_elapsed="-"
bar_char_total="="

# Toggle Bar border
bar_border="on"

# Progress bar length in spaces
bar_length=15

# Progress bar colors
#
bar_color_elapsed="distro"
bar_color_total="distro"

# Info display
# Display a bar with the info.
cpu_display="off"
memory_display="off"
battery_display="off"
disk_display="off"

# Backend Settings
# Image backend.
#
# Values:   'ascii', 'caca', 'chafa', 'jp2a', 'iterm2', 'off',
#           'termpix', 'pixterm', 'tycat', 'w3m', 'kitty'
image_backend="ascii"

# Image Source
#
# Which image or ascii file to display.
#
# Values:   'auto', 'ascii', 'wallpaper', '/path/to/img', '/path/to/ascii', '/path/to/dir/'
#           'command output (neofetch --ascii "$(fortune | cowsay -W 30)")'
#
# NOTE: 'auto' will pick the best image source for whatever image backend is used.
#       In ascii mode, distro ascii art will be used and in an image mode, your
#       wallpaper will be used.
image_source="auto"


# Ascii Options
# Which distro's ascii art to display.
#
# NOTE: AIX, Alpine, Anarchy, Android, Antergos, antiX, AOSC,
#       Apricity, ArcoLinux, ArchBox, ARCHlabs, ArchStrike,
#       XFerience, ArchMerge, Arch, Artix, Arya, Bedrock, Bitrig,
#       BlackArch, BLAG, BlankOn, BlueLight, bonsai, BSD,
#       BunsenLabs, Calculate, Carbs, CentOS, Chakra, ChaletOS,
#       Chapeau, Chrom*, Cleanjaro, ClearOS, Clear_Linux, Clover,
#       Condres, Container_Linux, CRUX, Cucumber, Debian, Deepin,
#       DesaOS, Devuan, DracOS, DragonFly, Drauger, Elementary,
#       EndeavourOS, Endless, EuroLinux, Exherbo, Fedora, Feren, FreeBSD,
#       FreeMiNT, Frugalware, Funtoo, GalliumOS, Gentoo, Pentoo,
#       gNewSense, GNU, GoboLinux, Grombyang, Guix, Haiku, Huayra,
#       Hyperbola, janus, Kali, KaOS, KDE_neon, Kibojoe, Kogaion,
#       Korora, KSLinux, Kubuntu, LEDE, LFS, Linux_Lite,
#       LMDE, Lubuntu, Lunar, macos, Mageia, MagpieOS, Mandriva,
#       Manjaro, Maui, Mer, Minix, LinuxMint, MX_Linux, Namib,
#       Neptune, NetBSD, Netrunner, Nitrux, NixOS, Nurunner,
#       NuTyX, OBRevenge, OpenBSD, OpenIndiana, OpenMandriva,
#       OpenWrt, osmc, Oracle, PacBSD, Parabola, Pardus, Parrot,
#       Parsix, TrueOS, PCLinuxOS, Peppermint, popos, Porteus,
#       PostMarketOS, Proxmox, Puppy, PureOS, Qubes, Radix, Raspbian,
#       Reborn_OS, Redstar, Redcore, Redhat, Refracted_Devuan, Regata,
#       Rosa, sabotage, Sabayon, Sailfish, SalentOS, Scientific, Septor,
#       SharkLinux, Siduction, Slackware, SliTaz, SmartOS, Solus,
#       Source_Mage, Sparky, Star, SteamOS, SunOS, openSUSE_Leap,
#       openSUSE_Tumbleweed, openSUSE, SwagArch, Tails, Trisquel,
#       Ubuntu-Budgie, Ubuntu-GNOME, Ubuntu-MATE, Ubuntu-Studio, Ubuntu,
#       Void, Obarun, windows10, Windows7, Xubuntu, Zorin, and IRIX
#       have ascii logos
# NOTE: Arch, Ubuntu, Redhat, and Dragonfly have 'old' logo variants.
#       Use '{distro name}_old' to use the old logos.
# NOTE: Ubuntu has flavor variants.
#       Change this to Lubuntu, Kubuntu, Xubuntu, Ubuntu-GNOME,
#       Ubuntu-Studio, Ubuntu-Mate  or Ubuntu-Budgie to use the flavors.
# NOTE: Arcolinux, Dragonfly, Fedora, Alpine, Arch, Ubuntu,
#       CRUX, Debian, Gentoo, FreeBSD, Mac, NixOS, OpenBSD, android,
#       Antrix, CentOS, Cleanjaro, ElementaryOS, GUIX, Hyperbola,
#       Manjaro, MXLinux, NetBSD, Parabola, POP_OS, PureOS,
#       Slackware, SunOS, LinuxLite, OpenSUSE, Raspbian,
#       postmarketOS, and Void have a smaller logo variant.
#       Use '{distro name}_small' to use the small variants.
ascii_distro="Parrot"

# Ascii Colors
#
# Example:
# ascii_colors=(distro)      - Ascii is colored based on Distro colors.
# ascii_colors=(4 6 1 8 8 6) - Ascii is colored using these colors.
ascii_colors=(distro)

# Bold ascii logo
# Whether or not to bold the ascii logo.
ascii_bold="on"


# Image Options
# Image loop
# Setting this to on will make neofetch redraw the image constantly until
# Ctrl+C is pressed. This fixes display issues in some terminal emulators.
#
image_loop="off"

# Thumbnail directory
thumbnail_dir="${XDG_CACHE_HOME:-${HOME}/.cache}/thumbnails/neofetch"

# Crop mode
crop_mode="normal"

# Crop offset
crop_offset="center"

# Image size
# The image is half the terminal width by default.
image_size="auto"

# Gap between image and text
gap=3

# Image offsets
yoffset=0
xoffset=0

# Image background color
background_color=


# Misc Options

# Stdout mode
# Turn off all colors and disables image backend (ASCII/Image).
# Useful for piping into another command.
# Default: 'off'
# Values: 'on', 'off'
stdout="off"
