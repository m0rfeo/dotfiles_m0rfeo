#!/bin/sh

# Parse output from ifconfig to check if wlo1 (wifi interface) exists
IFACE=$(/usr/sbin/ifconfig | grep wlo1 | awk '{print $1}' | tr -d ':')

# If exist print IP
if [ "$IFACE" = "wlo1" ]; then
    echo "%{F#ff00af}%{F#00ff00}$(/usr/sbin/ifconfig wlo1 | grep "inet " | awk '{print " "$2}')%{u-}"
else
    echo "%{F#ff00af}睊%{F#00ff00} Disconnected"
fi



