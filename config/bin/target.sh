#!/bin/sh

# tmp file to save target IP
TARGET=/tmp/target.txt

# If exist print IP and green skull
if [ -f $TARGET ]; then
    cat_target=$(cat $TARGET)
    echo "%{F#ff00af}什%{F#00ff00} $cat_target"
else
echo "%{F#ff00af}什%{F#00ff00} No Target"
fi
