#!/bin/sh

# Parse output from ifconfig to check if tun0 (vpn interface) exists
IFACE=$(/usr/sbin/ifconfig | grep tun0 | awk '{print $1}' | tr -d ':')

# If exist print IP
if [ "$IFACE" = "tun0" ]; then
    echo "%{F#FF00af}%{F#00ff00}$(/usr/sbin/ifconfig tun0 | grep "inet " | awk '{print " "$2}')%{u-}"
else
echo "%{F#FF00af}%{F#00ff00} Disconnected"
fi
