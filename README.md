# m0rfeo Dotfiles

## Requirements

- ne0fetch
- bspwm (as Windows Manager)
- sxhkd (to define shortcuts)
- rofi (simple menu to choose between applications)
- Hack Nerd Font (as font)
- polybar (bar with info on the top of the screen)
- picom (set transparency, opacity, shades...)
- i3lock (to lock the screen)
- zsh (as default interpreter)
- oh-my-zsh and powerlevel10k theme (framework to manage and customize zsh)
- Maybe you will need to install manually some of the plugins defined on .zshrc file
- fzf (command-line finder tool)
- ranger (command-line file manager)
- nvim (neovim)
- batcat (cat with improved output)
- lsd (ls with improved output)

## Getting started

Once you meet all the requirements you only have to put into your $HOME directory all these files with the structure provided here.
Remember that if you want to add this configuration for root or any other user you will have to do the same in his $HOME.

Structure: 
```
$HOME
├── .config
│
├── .p10k.zsh
│
├── .tmux.conf.local
│
└── .zshrc
```

## Expected Errors
- For sure if you copy all these files without modifying the paths of my system it will not work properly.
- In this case errors are your friend and will tell you what you need to install.

## Results

![Terminal Result](results/results_terminal.png)
![Wallpaper Result](results/results_wallpaper.png)

